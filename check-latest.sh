#!/bin/sh

# check if given submodule has a newer tag upstream
#
# ideally, renovate-bot should be able to do this on its own, but alas
# it only tracks commits:
# https://github.com/renovatebot/renovate/discussions/24890

set -e

submodule=themes/cstate

# fetch commit id recorded for the submodule
old_head=$(git -C $submodule/ rev-list HEAD --max-count=1)

# fetch and identify latest tag
git fetch
latest_tag_ref=$(git -C $submodule/ rev-list --tags --max-count=1)
latest_tag=$(git  -C $submodule/ describe --tags "$latest_tag_ref")
printf "submodule HEAD: %s\n" "$old_head"
printf "latest tag ref: %s\n" "$latest_tag_ref"
printf "latest tag: %s\n" "$latest_tag"


if [ "$old_head" = "$latest_tag_ref" ]; then
    printf "no new tag, all is well\n"
else
    printf "not running the latest, update submodule to %s\n" "$latest_tag"
    magic_command="(cd $submodule && git checkout master && git pull && git reset --hard $latest_tag) && git commit -m\"update cstate to $latest_tag\" $submodule"
    printf "that can be done with\n%s\n" "$magic_command"
    exit 1
fi
