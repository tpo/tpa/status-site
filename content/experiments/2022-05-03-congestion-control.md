---
title: Tor network with congestion control
date: 2022-05-03 16:00:00
#severity: ongoing
affected:
  - Network Experiments
section: experiment
---

## What changed?

We started deploying congestion control on the live Tor network.

Users of Tor versions 0.4.7.5-alpha and above will experience faster performance when using Exits or Onion Services that have upgraded to 0.4.7.5-alpha and above. This faster performance will also increase the Advertised Bandwidth of relays, similar to our previous flooding experiments. Thus, the overall capacity of the network will appear to increase on [our network metrics](https://metrics.torproject.org/bandwidth-flags.html).

As more clients upgrade, particularly after a Tor Browser Stable release with 0.4.7 is made, the consumed bandwidth of the network should also rise. We expect to make this Tor Browser Stable release on May 31st, 2022.

Our [performance metrics](https://metrics.torproject.org/torperf.html) will remain unchanged until we upgrade those onionperf instances to 0.4.7, which will likely also happen towards the end of May.

**Update, 2022-05-17 14:00UTC**: bastet has been running Tor 0.4.7.7 on its bandwidth authority since 2022-05-06 while we are still in the process of upgrading the network. While that won't influence the stablitiy of the network itself, it might create noise in bastet's votes.

## When did it start?

At 2022-04-25 19:00:00 UTC.

## When did it stop?

There is no clear date when we transitioned away from the experiment phase.  At the time of writing this (06/09/2022 13:30 UTC) roughly 82% of exit relays (by consensus weight) have upgraded to Tor 0.4.7. Additionally, we have upgraded our onionperf instances and Tor Browser starting with 11.0.13 (released on 05/23/2022) is shipping with Tor 0.4.7 as well.

## What potential problems to look for, and how to report them?

Relays might see [CPU overload](https://support.torproject.org/relay-operators/relay-bridge-overloaded/) in which case operators could consider to [rate-limit bandwidth](https://support.torproject.org/relay-operators/bandwidth-shaping/). Additionally, if operators pay for bandwidth by the gigabyte, they could [consider hibernation](https://support.torproject.org/relay-operators/limit-total-bandwidth/) if costs go up.

If you think something has gone wrong with enabling congestion control feel free to start a discussion on the [tor-relays](https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-relays) mailing list or a topic in our [forum](https://forum.torproject.net/c/support/relay-operator/17).
