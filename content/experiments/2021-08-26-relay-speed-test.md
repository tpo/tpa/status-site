---
title: Relay speed test
date: 2021-08-26 08:00:00
#severity: ongoing
affected:
  - Network Experiments
section: experiment
---

## What changed?

We are sending short bursts of traffic through every relay, to make
the relay capacity estimates more accurate. See the [mail thread on
tor-relays](https://lists.torproject.org/pipermail/tor-relays/2021-August/019786.html)
for more information.

## When did it start?

Around 2021-08-25 17:00:00 UTC.

## When did it stop?

Around 2021-09-08 13:00:00 UTC.

## What potential problems to look for, and how to report them?

Relays with a more accurate view of their own bandwidth could attract
higher traffic load, which could expose other configuration issues
(like not enough file descriptors available or not enough memory), so
be sure to watch your relay's logs for issues.

If you think something has gone wrong with the experiment, please write a
follow-up to the tor-relays@ announcement mail mentioned above.
