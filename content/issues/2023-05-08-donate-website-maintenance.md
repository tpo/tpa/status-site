---
title: donate website maintenance
date: 2023-05-08 02:00:00 +0000
resolved: true
resolvedWhen: 2023-05-08 04:45:00 +0000
# Possible severity levels: down, disrupted, notice
severity: down
affected:
  - donate.torproject.org
section: issue
---

Planned maintenance on the donate.torproject.org website will make it
inaccessible for a few hours.

The service should be restored at 2023-05-08 05:00:00 +0000.

For details, see https://gitlab.torproject.org/tpo/tpa/team/-/issues/41109
