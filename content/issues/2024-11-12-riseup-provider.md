---
title: Riseup network unreachable
date: 2024-11-12 04:00:00 +0000
resolved: true
resolvedWhen: 2024-11-12 20:20:00 +0000
# Possible severity levels: down, disrupted, notice
severity: down
affected:
  - Nextcloud
  - Most of Tails
section: issue
---

The network in which our Nextcloud instance and large sections of the
Tails infrastructure are hosted is currently unreachable.

Riseup is working with their upstream providers to resolve the issue,
but there is currently no clear indication when it will be resolved. More
information can be found on Riseup's own [status page][].

Update, 2024-11-12, 13:10 UTC: we have moved the Nextcloud instance to a
new IP address and it should become available again once DNS caches have
expired (this may take up to three hours).

[status page]: https://riseupstatus.net/issues/2024-11-11-partial-network-outage/
