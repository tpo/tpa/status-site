---
title: Nextcloud down
date: 2024-11-25 13:15:00 +0000
resolved: true
resolvedWhen: 2024-11-25 13:31:00 +0000
# Possible severity levels: down, disrupted, notice
severity: down
affected:
  - Nextcloud
section: issue
---

Tor's Nextcloud (https://nc.torproject.net) is down since at least Nov 25th
13:01 UTC. Our service provider (Riseup) has been notified and we'll update
this page as soon as we have more information.

UPDATE: Service is back, we don't yet have info about what happened.
