---
title: DNS services disruption
date: 2022-12-07 03:15:00
resolved: true
resolvedWhen: 2022-12-08 18:30:00
# Possible severity levels: down, disrupted, notice
severity: disrupted
affected:
  - DNS
section: issue
---

We are currently experiencing a higher than normal load on our DNS
infrastructure, and have received several reports of DNS resolution
failures, especially from users of Google DNS.

Update, 2022-12-08: We have deployed mitigations that appear to have
successfully restored the service to usual levels.

See [issue tpo/tpa/team#40996][] for details.

[issue tpo/tpa/team#40996]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40996
