---
title: donate website maintenance
date: 2024-10-02 16:00:00 +0000
resolved: true
resolvedWhen: 2024-10-02 18:40:00 +0000
# Possible severity levels: down, disrupted, notice
severity: notice
affected:
  - donate.torproject.org
section: issue
---

Donation site will be down for maintenance on Wednesday around 14:00
UTC, equivalent to 07:00 US/Pacific, 11:00 America/Sao_Paulo, 10:00
US/Eastern, 16:00 Europe/Amsterdam.

Update: maintenance was delayed by 2 hours, so this is now 09:00
US/Pacific, 13:00 America/Sao_Paulo, 12:00 US/Eastern, 16:00 UTC,
18:00 Europe/Amsterdam.

We're having [latency issues][] with the main donate site. We hope
that migrating it from our data center in Germany to the one in Dallas
will help fix those issues as it will be physically closer to the rest
of the cluster.

[latency issues]: https://gitlab.torproject.org/tpo/web/donate-neo/-/issues/134

Outage is expected to take no more than two hours, but no less than 15
minutes.

See the discussion issue for more information and feedback:

<https://gitlab.torproject.org/tpo/tpa/team/-/issues/41775>
