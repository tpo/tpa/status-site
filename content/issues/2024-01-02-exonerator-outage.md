---
title: ExoneraTor outage
date: 2024-01-02 19:25:00 +0000
resolved: true
resolvedWhen: 2024-02-07 15:36:56 +0000
# Possible severity levels: down, disrupted, notice
severity: down
affected:
  - ExoneraTor
section: issue
---

We are currently investigating an unusually high and sustained load on the ExoneraTor service, causing most queries to time out, resulting in a 502 "Proxy Error" page. Details and more context can be found in our [Gitlab bug tracker](https://gitlab.torproject.org/tpo/tpa/team/-/issues/41507) Sorry for the inconvenience.
