---
title: Tor Weather outage
date: 2024-03-19 15:00:00 +0000
resolved: true
resolvedWhen: 2024-03-25 08:58:00 +0000
# Possible severity levels: down, disrupted, notice
severity: down
affected:
  - weather.torproject.org
section: issue
---

We are in the process of upgrading Tor Weather to version 2.0. For more information about the details and expected downtime, see the [tracking ticket](https://gitlab.torproject.org/tpo/tpa/team/-/issues/41556). We are sorry for the inconvenience.
