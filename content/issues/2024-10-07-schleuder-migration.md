---
title: schleuder maintenance
date: 2024-10-07 12:00:00 +0000
resolved: true
resolvedWhen: 2024-10-07 12:02:00 +0000
# Possible severity levels: down, disrupted, notice
severity: notice
affected:
  - schleuder
section: issue
---

Schleuder lists administration will be down for maintenance on Monday
around 12:00 UTC, equivalent to 05:00 US/Pacific, 09:00 America/Sao_Paulo,
08:00 US/Eastern, 14:00 Europe/Amsterdam.

Schleuder lists will be [migrated to a new server][] with a more recent
schleuder version. Mail delivery should be unaffected, but changes in the
list configuration or keys made during the maintenance window may be lost.

[migrated to a new server]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/41796

The migration is expected to take no more than one hour, but no less than 15
minutes.

See the discussion issue for more information and feedback:

<https://gitlab.torproject.org/tpo/tpa/team/-/issues/41796>
