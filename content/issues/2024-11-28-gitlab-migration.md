---
title: GitLab migration to another machine cluster
date: 2024-11-28 18:00:00 +0000
resolved: true
resolvedWhen: 2024-11-29 14:04:00 +0000
# Possible severity levels: down, disrupted, notice
severity: notice
affected:
  - GitLab
section: issue
---

Starting on November 28th at 18:00 UTC, gitlab.torproject.org will be taken
offline in order to migrate it to our other machine cluster.

The transfer time is currently estimated at 18h, so gitlab will be coming online
on the next day, friday 29th. If we're lucky the transfer might finish sooner.

If you have any issues during that time, please reach out to us on IRC or via
email.
