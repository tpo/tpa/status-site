---
title: Gitlab and CollecTor outage
date: 2023-12-06 14:00:00 +0000
resolvedWhen: 2023-12-06 15:29:09 +0000
resolved: true
# Possible severity levels: down, disrupted, notice
severity: down
affected:
  - GitLab
section: issue
---

We've experienced heavy load and unresponsiveness on some of our
services (e.g. Gitlab and CollecTor) leading to outages and
disruptions. 

<del>The issue seems to have resolved itself, investigation seemed to show
this was a routing issue upstream.</del>

Update: issue have crept up again, root cause was 
elevated temperature with the hard drives on the affected
server. Upstream has replaced fans in the server and situation has
returned to normal.

See [issue tpo/tpa/team#41429](https://gitlab.torproject.org/tpo/tpa/team/-/issues/41429) for detailed analysis and updates.
