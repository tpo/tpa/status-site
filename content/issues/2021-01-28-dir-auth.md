---
title: Disruption of v3 onion services and consensus building
date: 2021-01-27 23:00:00
resolved: true
resolvedWhen: 2021-01-29 05:30:00
# Possible severity levels: down, disrupted, notice
severity: disrupted
affected:
  - Directory Authorities
  - v3 Onion Services
section: issue
---

We're currently facing [stability issues](https://lists.torproject.org/pipermail/network-health/2021-January/000661.html) with respect to our v3 onion services and consensus building. We are actively working on resolving those issues as soon as possible.
