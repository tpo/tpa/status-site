---
title: Tor Weather outage
date: 2023-03-30 12:41:00 +0000
resolved: true
resolvedWhen: 2023-03-31 08:46:35 +0000
# Possible severity levels: down, disrupted, notice
severity: down
affected:
  - weather.torproject.org
section: issue
---

Shortly after [launching](https://lists.torproject.org/pipermail/tor-relays/2023-March/021110.html) our new Tor Weather service the relay operator tornth [found a serious violation of privacy expectations](https://gitlab.torproject.org/tpo/network-health/tor-weather/-/issues/57) that resulted in suspending the Tor Weather service until that issue gets fixed. We are sorry for that inconvenience.
