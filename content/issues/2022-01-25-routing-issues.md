---
title: routing issues at main provider
date: 2022-01-25 06:00:00
resolved: true
resolvedWhen: 2022-01-27 18:58:00 +0000
# Possible severity levels: down, disrupted, notice
severity: disrupted
affected:
  - metrics.torproject.org
  - Tor Check
  - torproject.org
  - GitLab
  - Gitolite
  - Email
  - Mailing lists
section: issue
---

We are experiencing intermittent network outages that typically
resolve themselves within a few hours. Preliminary investigations seem
to point at routing issues at Hetzner, but we have yet to get a solid
diagnostic. We're following that issue in [issue 40601](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40601).
