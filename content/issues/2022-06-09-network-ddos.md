---
title: Network DDoS
date: 2022-06-09 14:00:00
resolved: true
resolvedWhen: 2023-05-09 09:00:00
# Possible severity levels: down, disrupted, notice
severity: disrupted
affected:
  - v3 Onion Services
section: issue
---

We are experiencing a network-wide DDoS attempt impacting the
performance of the Tor network, which includes both onion services and
non-onion services traffic. We are currently investigating potential
mitigations.

**Update, 2023-04-05**: The DDoS has significantly reduced in volume over the last month, although there are intermittent spikes that can still affect the performance of relays that get hit by them. Overall performance has improved, but can occasionally be slower when using affected relays. We are making significant progress on implementing our [Proof of Work defense](https://gitlab.torproject.org/tpo/core/tor/-/issues/40634), which should eliminate the incentive for much of these attacks. Other, more general DDoS defense work will happen after that.

**Update, 2022-10-25 12:00UTC**: There are several different kinds of overload going on that we are working on addressing. We are seeing performance degredation from an overload of exit connections and onion service circuit handshakes, causing our relays to deny circuit creations and degrade performance. Until we are able to determine mechanisms for rate limiting this activity, through development, experimentation, and testing, this DoS activity will continue to cause performance and reliability problems on the network. For details, see a [recent thread](https://lists.torproject.org/pipermail/tor-relays/2022-October/020858.html) on our tor-relays mailing list.

**Update, 2022-07-07 13:00UTC**: the overload we saw in the past few weeks seems
to be gone now and performance improved over the last couple of days. However,
the DDoS is not over yet, but changed its nature. We are currently investigating
how we can mitigate the new overload, which is affecting, e.g., our directory
authorities. For details, see a
[recent thread](https://lists.torproject.org/pipermail/tor-relays/2022-July/020686.html)
on our tor-relays mailing list.
